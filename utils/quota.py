
# -*- coding: utf8 -*-
import os

LABELS = {
   "/home":u"Dossier personnel",
   "/var/mail":u"Boite de réception"
}

def getFloat( chose ):
    chose = chose.replace(',', '.')
    return float(chose)

def getUserQuota( userLogin ):
    pipe = os.popen("sudo quota %s" % userLogin)
    string_result = pipe.read()
    pipe.close()
    string_result = string_result.split("\n")
    quotas = []
    for a_line in string_result[2:3]:
        usage, quota, limite, percentage, fs = a_line.split("\t")
        line_dict = {
           "label": "Quota personnel",
           "usage":getFloat(usage),
           "quota":getFloat(quota),
           "limite":getFloat(limite),
           "%":getFloat(percentage),
           "filesystem":"rda", # pourquoi pas ?
        }
        quotas.append(line_dict)
    return quotas



def fake_getUserQuota( userLogin ):
    return [
    {'%': 33.9, 
    'quota': 390.62, 
    'label': u'Dossier personnel (fake)',
    'limite': 585.94,
    'filesystem': '/home', 
    'usage': 420.32},                                                                    
    {'%': 0.1, 
    'quota': 100.00, 
    'label': u'Boite de r\xe9ception (fake)',
    'limite': 150.00, 
    'filesystem': '/var/mail', 
    'usage': 0.06}
    ]                                                
    
