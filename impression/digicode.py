#!/usr/bin/env python
# -*- coding: utf-8 -*-
# #############################################################
#                                            ..
#                       ....  ............   ........
#                     .     .......   .            ....  ..
#                   .  ... ..   ..   ..    ..   ..... .  ..
#                   .. .. ....@@@.  ..  .       ........  .
#              ..  .  .. ..@.@@..@@.  .@@@@@@@   @@@@@@. ....
#         .@@@@. .@@@@. .@@@@..@@.@@..@@@..@@@..@@@@.... ....
#       @@@@... .@@@.. @@ @@  .@..@@..@@...@@@.  .@@@@@.    ..
#     .@@@..  . @@@.   @@.@@..@@.@@..@@@   @@ .@@@@@@..  .....
#    ...@@@.... @@@    .@@.......... ........ .....        ..
#   . ..@@@@.. .         .@@@@.   .. .......  . .............
#  .   ..   ....           ..     .. . ... ....
# .    .       ....   ............. .. ...
# ..  ..  ...   ........ ...      ...
#  ................................
#
# #############################################################
"""
 digicode.py

     Fonctions pour controler le digicode du 4@J (simulation)

 Copyright (c) 2006, 2007, 2008, 2009 by Cr@ns (http://www.crans.org)
"""
import sys
import time
import tempfile
import os
import commands
import string
import random
import requests
if not '/usr/scripts' in sys.path:
    sys.path.append("/usr/scripts")
import cranslib.utils.files
import gestion.secrets_new as secrets_new

digicode_pass = secrets_new.get("digicode_pass")
# #############################################################
# CONSTANTES
# #############################################################
CODES_SERVERS = ["zamok", "asterisk"]
CODES_DIR = "/usr/scripts/var/digicode/"
CREATION_LINK = "https://intranet2.crans.org/digicode/create/"
LIST_LINK = "https://intranet2.crans.org/digicode/list/"
CERTIFICATE = "/etc/ssl/certs/cacert.org.pem"

# #############################################################
# EXCEPTIONS
# #############################################################
def CodeAlreadyExists(Exception):
    pass

# #############################################################
# FONCTIONS
# #############################################################
# test pour voir si on est bien sur la bonne machine
import socket
if socket.gethostname() not in CODES_SERVERS:
    CODES_DIR = tempfile.mkdtemp(prefix='fake_digicode')
    FAKE_DIR = True
    raise EnvironmentError("La manipulation des codes pour le digicode n'est possible que sur %s" % 
        ', '.join(CODES_SERVERS))
else:
    FAKE_DIR = False

def __del__():
    if FAKE_DIR:
        os.rmdir(CODES_DIR)


def __init__():
    pass
# ###############################
# save_code
# ###############################
# enregistre le codes pour user_name sur l'intranet2
#
def save_code(code, user_name):
    """enregistre le codes pour ``user_name``"""
    code = str(code)
    response = requests.post(CREATION_LINK + code, data={'password':digicode_pass, 'user':user_name}, verify=CERTIFICATE, timeout=2)
    try:
        code = int(response.content)
    except (TypeError, ValueError):
        raise ValueError(response.content)
    return code


# ###############################
# gen_code
# ###############################
# genere un code aleatoire
# et l'enregistre
#
def gen_code(user_name):
    """On contacte l'intranet 2 pour générer le code et on récupère le résultat"""
    response = requests.post(CREATION_LINK, data={'password':digicode_pass, 'user':user_name}, verify=CERTIFICATE, timeout=2)
    return response.content


# ###############################
# list_code
# ###############################
# liste les codes et leur age en secondes
#
def list_code(login=None):
    """
    Renvoie la liste des codes existants.
    La liste est sous la forme [(code, age (en sec), contenu du fichier),...]
    """
    response = requests.post(LIST_LINK + (login if login else ""), data={'password':digicode_pass}, verify=CERTIFICATE, timeout=2)
    code_list = []
    for line in response.content.split('\n'):
        if line:
            code_list.append(line.split(','))
    return code_list

def get_codes(login):
    return [code for (code, age, uid) in list_code(login)]

def read_code_file(code):
    """
    Lis le fichier correspondant au code.
    Renvoie le contenu du fichier.
    """
    myfile = open(os.path.join(CODES_DIR, code ), 'r')
    lineStr = myfile.readline()
    myfile.close()
    return lineStr.replace('\n','')


# ###############################
# menage
# ###############################
# supprime les codes vieux de plus de 24h
#
def menage():
    """
    Supprime les codes vieux de plus de 24h
    """
    fileList = os.listdir(CODES_DIR)
    for aFile in fileList:
        aFilePath = os.path.join(CODES_DIR, aFile)
        if os.path.isfile(aFilePath):
            if cranslib.utils.files.fileIsOlderThan(aFilePath, days=1):
                os.remove(aFilePath)
