#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
 etat_imprimante.py

     Récupère, filtre et formate l'état actuel de l'imprimante

 Copyright (c) 2006, 2007, 2008, 2009 by Cr@ns (http://www.crans.org)

"""

import sys
from hptools import snmp

class ErreurCommunication(Exception):
    """Si une erreur est survenue lors de la communication avec l'imprimante"""
    pass

def etat():
    return ["État inconnu. Les nounous y travaillent."]


