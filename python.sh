# À lancer par bash dans un shabang de la forme :
#!/bin/bash /usr/scripts/python.sh
#Ceci permet alors de lancer python avec un PYTHONPATH custom (/usr/scripts)
#pour éviter de l'importer dans chaque script
PYTHONPATH=/usr/scripts
for i in /usr/scripts/lib/python2.7/site-packages/*.egg; do
    PYTHONPATH=$PYTHONPATH:$i
done
PYTHONPATH=$PYTHONPATH:/usr/scripts/lib/python2.7/site-packages/
/usr/bin/env PYTHONPATH=$PYTHONPATH python "$@"
