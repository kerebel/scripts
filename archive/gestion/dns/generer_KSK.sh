#!/bin/bash
# Generation d'une nouvelle clef KSK 

DATE=`date --utc +%s`

cd /usr/scripts/var/dnssec

if [[ $1 == "" ]]
    then echo "Usage: $0 nom_de_la_zone"
	 echo "Exemple: $0 wifi.crans.org"
	 exit
fi

echo "Generation nouvelle KSK pour $1"
dnssec-keygen -f KSK -r /dev/urandom -a RSASHA256 -b 4096 -n ZONE $1

## On renomme de façon utilisable
mv K$1.+*.key K$1.$DATE.KSK.key
mv K$1.+*.private K$1.$DATE.KSK.private


## On met a jour les liens symboliques vers la clef actuelle
rm K$1.KSK.key
rm K$1.KSK.private

ln -s K$1.$DATE.KSK.key K$1.KSK.key
ln -s K$1.$DATE.KSK.private K$1.KSK.private

chmod 660 K*

## On genere une nouvelle ZSK
bash /usr/scripts/gestion/dns/generer_ZSK.sh $1

## On met à jour le fichier inclu dans la zone
bash generer_include_zone.sh $1

if [[ $1 == "crans.org" ]] 
    then echo "*** METTRE A JOUR LE CONDENSAT AUPRES DU REGISTRAR***"
	 echo "*** Les infos sont dans Kcrans.org.KSK.key ***"
fi

echo "*** Penser à supprimer l'ancienne clef dans quelques semaines ***"

