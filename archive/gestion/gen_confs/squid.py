#! /usr/bin/env python
# -*- coding: iso-8859-15 -*-

""" G�n�ration de la configuration pour squid sur sable """

import sys
sys.path.append('/usr/scripts/gestion')
from affich_tools import *
from gen_confs import gen_config
from ldap_crans import crans_ldap, ann_scol
from time import localtime
from config import bl_carte_et_actif, periode_transitoire

class squid(gen_config) :
    db = crans_ldap()
    actif = True # la sanction est elle active ?
    restart_cmd = '/etc/init.d/squid3 reload'

    def __str__(self) :
        return str(self.__class__).replace('_','-').split('.')[2]

    def _gen(self) :
        self._mklist()

    def _mklist(self) :
        fic = self._open_conf(self.FICHIER)

        if not self.actif:
            return

        # recherche dans la base LDAP
        if '=' in self.chaine:
            # chaine de tri sp�ciale
            liste = self.db.search("paiement=ok&%s"%self.chaine)
            l_proprio = liste["adherent"] + liste["club"]
            l_machine = liste["machine"]

        else:
            # recherche dans les adh�rents blacklist�s
            liste = self.db.search("paiement=ok&ablacklist=*%s*"%self.chaine)
            l_proprio = liste["adherent"] + liste["club"]
            l_proprio = [ x for x in l_proprio if self.chaine in x.blacklist_actif() ]

            # recherche dans les machines blacklist�s
            liste = self.db.search("paiement=ok&mblacklist=*%s*"%self.chaine)
            l_machine = liste["machine"]
            l_machine = [ m for m in l_machine if self.chaine in m.blacklist_actif() ]

        # on ajoute les machines des proprios aux autres machines
        for proprio in l_proprio :
            l_machine += proprio.machines()

        # on colle toutes les machines dans le fichier
        self.anim.iter=len(l_machine)
        for m in l_machine:
            self.anim.cycle()
            fic.write( "\n# %s\n%s\n" % (m.Nom(), m.ip()))
        fic.close()

class squid_check(gen_config):
    """Classe de v�rification des blacklistes � reg�n�rer pour une IP."""

    db = crans_ldap()
    actif = True # la sanction est elle active ?
    restart_cmd = '/etc/init.d/squid3 reload'

    def __str__(self) :
        return str(self.__class__).replace('_','-').split('.')[2]

    def __init__(self, ips):
        self.ips = ips

    def _gen(self):
        # Reg�n�ration des fichiers des blacklistes correspondant aux
        # nouvelles IPs

        # On regarde pour chaque IP la liste des blacklistes � reg�n�rer
        to_regen = []
        for ip in self.ips:
            s = self.db.search('ipHostNumber=%s' % ip)['machine']
            if len(s) > 0:
                m = s[0]
                p = m.proprietaire()
                to_regen.extend(m.blacklist_actif())
                if (bl_carte_et_actif and not periode_transitoire and ann_scol
                        not in p.carteEtudiant()):
                    to_regen.append('carte')
                if p.chbre() == '????':
                    to_regen.append('chbre')

        # On ne prend chaque blackliste qu'une seule fois
        to_regen_uniq = []
        for item in to_regen:
            if item == 'mail_invalide':
                item = 'mail'
            if item not in to_regen_uniq:
                to_regen_uniq.append(item)
        self.anim.reinit()
        print OK

        # On reg�n�re les fichiers pour chaque blackliste
        for item in to_regen_uniq:
            curclass = eval("squid_%s()" % item)
            curclass.anim = anim('\tgeneration blackliste %s' % item)
            curclass._gen()
            curclass.anim.reinit()
            print OK

    def gen_conf(self) :
        # Red�fini pour enlever un peu de cosm�tique g�nante
        """ G�n�ration des fichiers de conf, retourne False si erreur """
        self.lock()
        self.anim = anim('\tverification blacklistes')
        try :
            self._gen()
            self.unlock()
            return True
        except :
            self.anim.reinit()
            self._restore()
            self.unlock()
            return False

class squid_upload(squid) :
    """ G�n�re le fichier blacklist-upload pour squid """
    FICHIER = "/etc/squid3/blacklist_upload"
    chaine = "upload"

class squid_p2p(squid) :
    """ G�n�re le fichier blacklist-p2p pour squid """
    FICHIER = "/etc/squid3/blacklist_p2p"
    chaine = "p2p"

class squid_autodisc_upload(squid) :
    """ G�n�re le fichier blacklist-autodiscupload pour squid """
    FICHIER = "/etc/squid3/blacklist_autodisc_upload"
    chaine = "autodisc_upload"

class squid_autodisc_p2p(squid) :
    """ G�n�re le fichier blacklist-autodisc-p2p pour squid """
    FICHIER = "/etc/squid3/blacklist_autodisc_p2p"
    chaine = "autodisc_p2p"

class squid_autodisc_virus(squid) :
    """ G�n�re le fichier blacklist-virus pour squid """
    FICHIER = "/etc/squid3/blacklist_autodisc_virus"
    chaine = "autodisc_virus"

class squid_virus(squid) :
    """ G�n�re le fichier blacklist-virus pour squid """
    FICHIER = "/etc/squid3/blacklist_virus"
    chaine = "virus"

class squid_warez(squid) :
    """ G�n�re le fichier blacklist-warez pour squid """
    FICHIER = "/etc/squid3/blacklist_warez"
    chaine = "warez"

class squid_bloq(squid) :
    """ G�n�re le fichier blacklist-bloq pour squid """
    FICHIER = "/etc/squid3/blacklist_bloq"
    chaine = "bloq"

class squid_carte(squid) :
    """ G�n�re le fichier blacklist-carte pour squid """
    actif = bl_carte_et_actif and not periode_transitoire
    if not actif : restart_cmd = ''
    FICHIER = "/etc/squid3/blacklist_carte_et"
    chaine = "carteEtudiant!=%i"%ann_scol

class squid_chbre(squid) :
    """ G�n�re le fichier blacklist-chbre pour squid """
    FICHIER = "/etc/squid3/blacklist_chbre"
    chaine = "chbre=????"

class squid_mail(squid) :
    """ G�n�re le fichier blacklist-mail pour squid """
    FICHIER = "/etc/squid3/blacklist_mail"
    chaine = "mail_invalide"
