#!/bin/sh
# Mise � jour de l'arborescence du site web
# Execute toutes les minutes (par root).

OQP=/var/lock/cvsweb
CHEMIN=/var/local/cvsweb
REPOSITORY=/home/httpd/CVS-Repository/ 

if [ $CHEMIN/commit.todo -nt $CHEMIN/commit.ok ]; then
  if ! [[ -f $OQP ]]; then
    chown -R www-data:webcvs $REPOSITORY
    chmod -R o-rwx $REPOSITORY
    touch $OQP
    cd /home/httpd/web
    su www-data -c "/usr/bin/cvs -d $REPOSITORY update -P -d" >/dev/null 2>&1
    test -s $OQP || rm -f $OQP
    su www-data -c "touch $CHEMIN/commit.ok"
  fi
fi
