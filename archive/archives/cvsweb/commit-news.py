#!/usr/bin/python
# -*- coding: iso-8859-1 -*-
#
# Hack par CC 13/07/1999 pour envoyer sur crans.cvs-checkins un 
# compte-rendu d'activit� de ce qui se passe...
#
# modif par SamK 22/11/99: splitter les lignes, sinon nntplib refuse le message
# si ya des lignes trop longues..
#
# modif par OS 15/05/00 ajout d'un Reply-To...
# 
# Ajouts par NS 13/02/03 : signature qui explique un peu ce qui se passe, et 
# cr�ation automatique d'un lien vers la page webcvs qui pr�sente le diff.
#
# Modif par Benoit 11/12/04 : changement du repertoire ou sont loggue les
# commits et fermeture du fichier
#
# Intimement li� � CVSROOT/loginfo
#

import nntplib,string,os,StringIO

def LFsplit(s):
    res=""
    while len(s)>80 :
        pos=string.find(s,' ',65,80)
        if pos < 0 :
                res=res+s[0:75]+'\n'
                s=s[75:len(s)+1]
        else :
                res=res+s[0:pos]+'\n'
                s=s[pos+1:len(s)+1]
    if len(s)>0:
        res=res+s
    return res

for uid in os.listdir('/var/local/cvsweb/checkins/'):
    try:
        so = []
        fi = open('/var/local/cvsweb/checkins/'+uid,'r')

        so.append("From: CVS admin <root@crans.org>\n" + \
                  ("Subject: CVS Commit par %s\n" % uid)+ \
                  "Organization: Crans Internet Site\n" + \
                  "Newsgroups: crans.cvs-checkins\n" + \
                  "Reply-To: Nounous <nounou@crans.org>\n" + \
                  "Followup-To: crans.informatique\n" )

        so.append("MIME-Version: 1.0\n" + \
                  "Content-Type: text/plain\n")
        so.append("\n")
        envoie = 0
        links_base="http://www.crans.org/cgi-bin/cvsweb"
        while 1:
            s = fi.readline()
            if not s: break
            envoie = 1
            links=['Liens vers les diffs sur cvsweb :\n']
            diffok = 0
            
            # On essaye de cr�er les liens vers cvsweb automatiquement.
            try:
                if s[0:3]=="web":
                    link=string.split(s)
                    for file in link[1:]:
                        file=string.split(file,",")
                        if not string.split(file[0],".")[-1] in ['jpg','jpeg','gif','png']:
                                links.append("%s/%s/%s.diff?r1=%s&r2=%s\n" % (links_base,link[0],file[0],file[1],file[2]))
                                diffok = 1
                    # S'il y a au moins un diff affichable, on donne le lien.
                    if diffok:
                        links.append('\n')
                        s=string.join(links,'')
                else:
                    s=LFsplit(s)
                so.append(s)
            except:
                so.append(s)
        # Avec une signature c'est plus propre.
        so.append("\n" + \
                  "-- \n" + \
                  "Notification automatique des CVS commits sur le site du CR@NS.\n"+ \
                  "Voir http://www.crans.org/cgi-bin/cvsweb/web/ pour acc�der aux archives CVS compl�tes.")


            # maintenant, on envoie : 

        if envoie:
            sso = string.join(so,'')
            fso = StringIO.StringIO(sso)
            nntplib.NNTP('news.crans.org').post(fso)
            #print sso
        fi.close()
        os.unlink('/var/local/cvsweb/checkins/'+uid)
    except:
            raise

