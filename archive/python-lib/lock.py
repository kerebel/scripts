#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-

""" Gestion de lock 

Copyright (C) Fr�d�ric Pauget
Licence : GPLv2
"""

import os,string,time,sys, affich_tools
from commands import getoutput
from user_tests import getuser

def make_lock(lock_file, lock_comment='',nowait=1) :
    """ Cr�ation d'un lock 
    si nowait=0 fait un sys.exit(254) quand un ancien lock actif est rencontr�
    """
    if os.path.isfile(lock_file) :
	### Lock existant
	
	# Lecture du lock
	fd = open(lock_file, "r")
	pid= fd.readline().strip()
	user = fd.readline().strip()
	fd.close()
	    
	# Informations sur le processus lockant
	if os.system( "ps %s > /dev/null 2>&1" % pid ) :
	    # Le script lockant ne tourne plus
	    os.remove(lock_file)
	elif nowait :
	    sys.stderr.write('Lock : %s\n' % lock_file)
	    l=getoutput('ps -o etime --no-headers %s' % pid)
	    data = [ user , pid , l.strip() ]
	    
	    # Formatate de etime
	    s = data[-1].split('-')
	    if len(s)==2 :
		txt = '%s jour(s) ' % s[0]
		s=s[1]
	    else : 
		txt = ''
		s=s[0]
		
	    s = s.split(':')
	    if len(s) == 3 :
		txt = '%sh%smin%ss' % tuple(s)
	    elif len(s) == 2 :
		txt = '%smin%ss' % tuple(s)
	    else :
		txt = '???'

	    data[-1]=txt
		
	    sys.stderr.write('\tpropri�taire : %s\n\tpid : %s\n\td�marr� depuis %s\n' % tuple(data) )
	    sys.exit(254)
	else :
	    # Il faut attendre
	    a = affich_tools.anim('\tattente du lock')
	    for i in range(8) :
		time.sleep(1)
		a.cycle()
	    sys.stdout.write('\r')
	    return make_lock(lock_name, lock_comment)
	    
    ### Prise du lock
    lock_fd=open(lock_file, "w")
    lock_fd.write("%s\n%s\n%s" % (os.getpid(), getuser(), lock_comment) )
    lock_fd.close()

def remove_lock( lock_file ) :
    """ Destruction du lock """
    try :
	fd = open(lock_file, "r")
	if fd.readline().strip()=="%s" % os.getpid():
	    os.remove(lock_file)
	    fd.close()
    except :
	None
