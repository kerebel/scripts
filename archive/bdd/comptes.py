from sqlobject import *
#from comptes import CompteUnix

class Attribut(SQLObject):
    nom = UnicodeCol()
    url_desc = StringCol(default=None)
    permissions = RelatedJoin('Permission')
    groupes_unix = RelatedJoin('GroupeUnix')
    comptes = RelatedJoin('CompteUnix')
    
class Permission(SQLObject):
    nom = UnicodeCol()
    attributs = RelatedJoin('Attribut')

class CompteUnix(SQLObject):
    proprio = ForeignKey('Proprio')
    solde = FloatCol()
    attributs = RelatedJoin('Attribut')

    # Champs standards 
    username = StringCol(unique=True)
    passwd = UnicodeCol()
    uid = IntCol(unique=True)
    homedir = StringCol()
    shell = StringCol()
    gecos = StringCol()
    pwdexpire = DateTimeCol(default=None)
    gid = IntCol()
    
class GroupeUnix(SQLObject):
    gid = IntCol(unique=True)
    nom = StringCol(unique=True)
    descr = UnicodeCol()
    passwd = StringCol(default='!')
