#!/bin/sh
#
# active_sql.sh
#
# Syntaxe : active_sql login pass
#
# Cr�ation d'une base mysql nomm�e web_login
#
# Djoume - 29/06/2002
#

CSQL="/usr/bin/mysql -p "

PASSWORD=/etc/passwd

if [ 2 -ne "$#" ] ; then
	echo "Cr�ation d'une base mysql"
	echo "Erreur : il faut 2 arguments"
	echo "Syntaxe :"
	echo "active_sql.sh login pass"
	echo "La base cr�� s'appellera 'web_login'"
	exit 1
fi

nom_base="web_$1"

echo "Cr�ation de la base $nom_base"
echo "login : $1"
echo "pass  : $2"

# On verifie que l'utilisateur existe

if awk 'BEGIN{FS=":"}{print $1}' < $PASSWORD | grep -w -e $1 > /dev/null; then
	echo "$1 existe dans $PASSWORD."
else echo "Attention $1 n'existe pas dans $PASSWORD !"
fi

echo "Continuer ? (o/n)"
read rep;
case $rep in
	y | Y | o | O)
		echo "Cr�ation de la base..."
		$CSQL -e "create database $nom_base;"
		if [ 0 -ne $? ]; then 
			echo "Erreur lors de la cr�ation de la base." 
			exit 1 
		fi
		echo "Attribution des privili�ges..."
		$CSQL -e "grant SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, REFERENCES, INDEX, ALTER on $nom_base.* to $1@localhost;"
		if [ 0 -ne $? ]; then 
			echo "Erreur lors l'attribution des privileges." 
			exit 1 
		fi
		echo "Attribution du mot de passe..."
		$CSQL -e "set password for $1@localhost=password('$2');"
		if [ 0 -ne $? ]; then 
			echo "Erreur lors l'attribution du mot de passe." 
			exit 1 
		fi
		echo "Done."
		;;
	*)
		echo "Annulation..."
		exit 0
		;;
esac

exit 0
