#! /usr/bin/env python
# -*- encoding: iso-8859-15 -*-

#generation d'une question de questionnaire si il y a des arguments, sinon affiche le résultat

class questionnaire:
    execute = 0
    liste_questions = []
    min_point = 0
    max_point = 0
    fonction_affiche = True

def int_try(point):
    try:
        return int(point)
    except ValueError :
        return 0

def parse(text):
    ligne = text.split("\\")
    question = []
    for l in ligne:
        l = l.split(":",1)
        if len(l)>=2:
            question+=[(l[0],l[1])]
    return question

def question_choix_unique(f,QR,quest):
    r=""
    id = QR[0][0]
    r+=f.rawHTML("""
    <p> %(id)i : %(Q)s <br>
    """ % { "id" : quest.execute , "Q" : QR[0][1]})
    QR=QR[1:]
    max = 0
    min = 0
    for (point,rep) in QR:
        point = int_try(point)
        if (max < point):
            max = point
        if (min > point):
            min = point
        r+=f.rawHTML("""<input type="radio" name="radio%(id)i" onclick="ajoute(%(id)i,%(point)i)" > %(rep)s<br>\n"""% { "point" : point , "id" : quest.execute, "rep" : rep})
    quest.max_point += max
    quest.min_point += min
    r += f.rawHTML("\n</p>\n")
    return r


def question_choix_multiple(f,QR,quest):
    r = ""
    r +=f.rawHTML("<p> %(id)i : %(Q)s <br>\n" % { "id" : quest.execute , "Q" : QR[0][1]})
    QR=QR[1:]
    for (point,rep) in QR:
        point = int_try(point)
        if point > 0:
            quest.max_point += point
        else:
            quest.min_point += point
        r+=f.rawHTML("""<input type="checkbox" name="radio%(id)i" onclick="ajoute_multiple(%(point)i,this.checked)" > %(rep)s<br>\n"""% { "point" : point , "id" : quest.execute, "rep" : rep})
    r +=f.rawHTML("\n</p>\n")
    return r

def fonction_javascript(f,quest):
    if (quest.fonction_affiche==True):
        quest.fonction_affiche = False
        return f.rawHTML("""
<script type="text/javascript">
    var point_globale = 0
    var pointQ = new Array(%(nb_quest)i)
    var i=0
    function update()
    {
    var result_p = document.getElementsByName('result_p')
    var point_result_p = Math.round((point_globale-(%(min_point)i)) * 10000 / %(intervalle)i)/100
    for (var i=0;i<result_p.length;i++)
    {
      result_p[i].innerHTML = point_result_p
    }
    var result = document.getElementsByName('result')
    for (var i=0;i<result.length;i++)
    {
      result[i].innerHTML = point_globale
    }
    }
    function ajoute_multiple(point,on)
    {
      if(on)
        {point_globale = point_globale + point}
      else
        {point_globale = point_globale - point}
      update()
    }
    function ajoute(id_Q,point)
    {
      if(isNaN(pointQ[id_Q])){pointQ[id_Q]=0}
      point_globale = point_globale + point - pointQ[id_Q]
      pointQ[id_Q] = point
      update()
    }
</script>
""" % {"min_point": quest.min_point,
       "intervalle": quest.max_point-quest.min_point,
       "nb_quest": quest.execute})
    else:
        return ""

def result(f,quest):
    return f.rawHTML("""<span name="result"> 0 </span>""")

def result_pourcent(f,quest):
    return f.rawHTML('<span name="result_p"> 0 </span><script type="text/javascript">update()</script>')

def result_max(f,quest):
    return f.text("%i" % quest.max_point)

def result_min(f,quest):
    return f.text("%i" % quest.min_point)

def execute(macro,text):
    try:
        macro._macro_questionnaire.execute +=1
    except :
        macro._macro_questionnaire = questionnaire()

    f = macro.formatter
    quest = macro._macro_questionnaire

    if text == None:
        return fonction_javascript(f,quest) + result(f,quest)
    elif text == "%":
        return fonction_javascript(f,quest) + result_pourcent(f,quest)
    elif text == "M":
        return fonction_javascript(f,quest) + result_max(f,quest)
    elif text == "m":
        return fonction_javascript(f,quest) + result_min(f,quest)
    else:
        QR = parse(text)
        if QR[0][0][0]=='*':
            return question_choix_multiple(f,QR,quest)
        else:
            return question_choix_unique(f,QR,quest)
