-----------------------------------------------------------------
-- Maintenance de la base netacct-ng sur pgsql, lancé par cron
-----------------------------------------------------------------

-- effacement des vieux enregistrements
DELETE FROM upload where date< timestamp 'now' - interval '2 days';

-- suppression complète des entrées
VACUUM;

-- réindexation des tables
REINDEX TABLE upload;
