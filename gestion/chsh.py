#! /usr/bin/env python
# -*- coding: utf-8 -*-

""" Changement du shell de l'utilisateur lancant le programme
    DOIT ETRE LANCE PAR SUDO

    Copyright (C) Frédéric Pauget
    Licence : GPLv2
"""
import os, sys

from affich_tools import prompt
from ldap_crans import crans_ldap

db = crans_ldap()
uid = os.getenv('SUDO_UID')
if not uid :
    print "Impossible de déterminer l'utilisateur"
    sys.exit(1)

s = db.search('uidNumber=%s' % os.getenv('SUDO_UID'),'w')

# On vérifie que c'est pas un club
club = s['club']
if len(club) == 1 :
    print 'Pas de changement de shell pour les clubs'
    sys.exit(2)

# On regarde si on a des résultats dans les adhérents
adh = s['adherent']
if len(adh) != 1 :
    print 'Erreur fatale lors de la consultation de la base LDAP'
    sys.exit(3)

adh = adh[0]
shell = prompt(u'Nouveau shell :')
fd=open('/etc/shells')
lines=fd.readlines()
fd.close()

shells = [line.strip() for line in lines if not (line.startswith('#') or line.endswith('/rssh'))]

if not shell in shells:
    print 'Shell invalide. Les shells valides sont :'
    print '\n'.join(shells)
    sys.exit(4)

adh.chsh(shell)
adh.save()
# A cause de nscd
print "La modification sera prise en compte dans l'heure suivante."
