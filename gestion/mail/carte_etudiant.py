#!/bin/bash /usr/scripts/python.sh
# -*- coding: utf-8 -*-

import sys
import smtplib
from gestion import config
from gestion.affich_tools import cprint
from gestion import mail
import lc_ldap.shortcuts

# Attention, si à True envoie effectivement les mails
SEND=False
deadline="mardi 5 novembre"
ldap_filter=u"(&(paiement=%(annee)s)(!(carteEtudiant=%(annee)s))(!(etudes=Personnel ENS))(aid=*))" % {'annee': config.ann_scol}

conn=lc_ldap.shortcuts.lc_ldap_readonly()
mailaddrs=set()
for adh in conn.search(ldap_filter):
    if 'canonicalAlias' in adh.attrs.keys():
        mailaddrs.add(str(adh['canonicalAlias'][0]))
    elif 'mail' in adh.attrs.keys():
        mailaddrs.add(str(adh['mail'][0]))
    else:
        raise ValueError("%r has nor mail nor canonicalAlias, only %s" % (adh, adh.attrs.keys()))

#mailaddrs=["David.Marshall@crans.org", "Bertrand.Bryche@crans.org", "Karima.Toiybou@crans.org", "Thomas.Marchandier@crans.org"]
if not SEND:
    print "Va envoyer le message à %s personnes. Mettre la variable SEND à True effectuer l'envoie" % len(mailaddrs)
    sys.exit(0)

echecs=[]
conn_smtp=smtplib.SMTP('smtp.adm.crans.org')
for To in mailaddrs:
    cprint(u"Envoi du mail à %s" % To)
    mailtxt=mail.generate('carte_etudiant', {'deadline':deadline, 'To':To}).as_string()
    try:
        if SEND:
            conn_smtp.sendmail("cableurs@crans.org", (To,), mailtxt)
    except:
        cprint(u"Erreur lors de l'envoi à %s " % To, "rouge")
        echecs.append(To)

conn_smtp.quit()

if echecs:
    print "\nIl y a eu des erreurs :"
    print echecs
