#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''Ce script permet de savoir s'il y a du monde au 2B.'''

import sys
sys.path.append('/usr/scripts/gestion')
from ldap_crans import crans_ldap
from hptools import hpswitch, ConversationError
from affich_tools import coul, cprint
from whos import aff
from socket import gethostname
from os import system

def liste_2b():
    '''Cette fonction permet de savoir s'il y a du monde au 2B.'''

    sw = hpswitch('backbone.adm')
    db = crans_ldap()

    ## Rappel (faut bien le mettre quelque part ...) :
    # Quand ça marche pas, c'est sûrement quelque chose parmi :
    # -snmpwalk n'est pas installé (paquet Debian snmp)
    # -les MIBs ne sont pas installés (/etc/snmp/mibs/hp)
    # -snmpwalk ne sait pas qu'il faut aller les chercher dans ce dossier
    #   (/etc/snmp/snmp.conf)
    macs = sw.show_prise_mac(33) #B9, ex-(D9->87)

    # Machines habituellement présentes, s'inquiéter si on ne les voit plus:
    expected = {
        "00:07:cb:b1:99:4e": "Freebox",
        "dc:9f:db:07:da:c5": "helene (wifi)",
        "00:15:6d:ef:68:b7": "promethee (wifi)",
        "00:1c:2e:56:1a:20": "minigiga (switch)",
        "00:40:8c:7f:4a:b5": "tinybrother",
        "00:24:8c:44:3b:70": "vo",
    }

    # Machines branchée sur la prise
    # (on voit aussi les machines branchées via une borne WiFi du 2B)
    machines = []
    if macs:
        machines_crans = []
        for mac in macs:
            fm = db.search("mac=%s" % mac)
            if len(fm['machine']) != 0:
                m = fm['machine'][0]
                # Machine personnelle ou machine Cr@ns ?
                if len(fm['borneWifi']) == 0 and len(fm['machineCrans']) == 0:
                    try:
                        p = m.proprietaire()
			if p.idn == "cid" and p.id() == "35":
                            # Machine du *club* Cr@ns
                            machines_crans.append(m)
                            continue
                        if len(p.droits()) != 0 or len(fm['machineFixe']) != 0:
                            # On filtre en Wifi sur ceux qui ont des droits
                            machines.append(m)
                    except:
                        pass
                else:
                    machines_crans.append(m)
            elif mac not in expected:
                    cprint("Machine inconnue: %s" % mac, 'rouge')
        for expectedMac in expected.iterkeys():
            if expectedMac not in macs:
                cprint("Machine %s manquante !" % expected[expectedMac], 'rouge')
        # Affichage des machines
        cprint('---=== Machines du Crans ===---', 'bleu')
        aff(machines_crans, mtech=True)
        cprint('---=== Autres machines ===---', 'bleu')
        aff(machines)
    ttyfound = 1

    # Utilisateurs connectés sur vo sur place
    if gethostname() == 'vo':
        cprint('---=== W(ho) sur vo ===---', 'bleu')
        ttyfound = system("/usr/bin/w -s | grep tty`fgconsole`")
    print ''

    # Conclusion
    if len(machines) != 0 or ttyfound == 0:
        cprint("---=== Il y a du monde au 2B ! ===---", 'vert')
    else:
        cprint("---=== Il semble n'y avoir personne au 2B ... ===---", 'rouge')
    
if __name__ == '__main__':
    liste_2b()
