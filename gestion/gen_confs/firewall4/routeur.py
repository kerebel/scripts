#!/usr/bin/env python
# -*- coding: utf-8 -*-
import utils
import base

from utils import pretty_print, OK, anim
from base import dev

class firewall(base.firewall):
    """
    Pare-feu de la machine virtuelle routeur, gérant essentiellement le portail captif
    et le routage des "petit" vlan (isolement, accueil…)
    """

    def __init__(self):
        super(self.__class__, self).__init__()

        self.reloadable.update({
            'portail_captif_route' : self.portail_captif_route,
            'portail_captif' : self.portail_captif,
        })

        self.use_ipset.extend([])
        self.use_tc.extend([])

    def raw_table(self):
        table = 'raw'
        super(self.__class__, self).raw_table()
        return

    def mangle_table(self):
        table = 'mangle'
        super(self.__class__, self).mangle_table()
        return

    def filter_table(self):
        table = 'filter'
        super(self.__class__, self).filter_table()

        chain = 'FORWARD'
        self.flush(table, chain)
        self.add(table, chain, '-j %s' % self.portail_captif_route(table))
        return

    def nat_table(self):
        table = 'nat'
        super(self.__class__, self).raw_table()

        chain = 'PREROUTING'
        self.add(table, chain, '-j %s' % self.portail_captif(table))

        chain = 'POSTROUTING'
        self.add(table, chain, '-j %s' % self.portail_captif_route(table))
        return

    def portail_captif_route(self, table=None, apply=False):
        """PNAT les (ip,port) à laisser passer à travers le portail captif"""
        chain = 'CAPTIF-ROUTE'

        if table == 'filter':
            pretty_print(table, chain)
            for ip in base.config.accueil_route.keys():
                for type in base.config.accueil_route[ip].keys():
                    if type in ['udp', 'tcp']:
                        self.add(table, chain, '-p %s -d %s -m multiport --dports %s -j ACCEPT' % (type, ip, ','.join(base.config.accueil_route[ip][type])))
                        self.add(table, chain, '-p %s -s %s -m multiport --sports %s -j ACCEPT' % (type, ip, ','.join(base.config.accueil_route[ip][type])))
            self.add(table, chain, '-j REJECT')
            print OK

        if table == 'nat':
            pretty_print(table, chain)
            #intranet et wiki pour le vlan accueil
            for ip in base.config.accueil_route.keys():
                for type in base.config.accueil_route[ip].keys():
                    if type in ['udp', 'tcp']:
                        for net in base.config.NETs['accueil']:
                            self.add(table, chain, '-s %s -p %s -d %s -m multiport --dports %s -j MASQUERADE' % (net, type, ip, ','.join(base.config.accueil_route[ip][type])))
                        for net in base.config.NETs['isolement']:
                            self.add(table, chain, '-s %s -p %s -d %s -m multiport --dports %s -j MASQUERADE' % (net, type, ip, ','.join(base.config.accueil_route[ip][type])))
            for net in base.config.NETs['personnel-ens']:
                self.add(table, chain, '-i %s -s %s -j MASQUERADE' % (dev['app'], net))
            print OK

        if apply:
            self.apply(table, chain)
        return chain

    def portail_captif(self, table=None, apply=False):
        """Redirige vers le portail captif"""
        chain = 'PORTAIL-CAPTIF'

        if table == 'nat':
            pretty_print(table, chain)
            for ip in base.config.accueil_route.keys():
                for type in base.config.accueil_route[ip].keys():
                    if type in ['udp', 'tcp']:
                        self.add(table, chain, '-p %s -d %s -m multiport --dports %s -j RETURN' % (type, ip, ','.join(base.config.accueil_route[ip][type])))

            for net in base.config.NETs['isolement']:
                self.add(table, chain, '-p tcp -s %s --destination-port 80 -j DNAT --to-destination 10.52.0.10' % net)

            for net in base.config.NETs['accueil']:
                self.add(table, chain, '-p tcp -s %s --destination-port 80 -j DNAT --to-destination 10.51.0.10' % net)
                self.add(table, chain, '-p udp -s %s --dport 53 -j DNAT --to 10.51.0.10' % net)
                self.add(table, chain, '-p tcp -s %s --dport 53 -j DNAT --to 10.51.0.10' % net)
            print OK

        if apply:
            self.apply(table, chain)
        return chain


