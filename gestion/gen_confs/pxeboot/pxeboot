#!/bin/bash -e
#
# pxeboot.sh: création d'un répertoire TFTP de boot par PXE
# Copyright (C) 2008, Nicolas Dandrimont <Nicolas.Dandrimont@crans.org>
#
# Utilisation : pxeboot.sh IP_SRV_TFTP

# Définitions communes

. config

rm -rf $TFTPROOT || exit 1

####################################
# Vérifications
####################################
for type in $UBUNTU_LIVE_TYPE; do
	for dist in $UBUNTU_LIVE; do
	    for arch in $UBUNTU_LIVE_ARCHS; do
	        if ! test -e $ISODIR/ubuntu/$type-$dist-desktop-$arch.iso 
	        then echo "$ISODIR/ubuntu/$type-$dist-desktop-$arch.iso n'existe pas" && sleep 3;
	        fi
	    done
	done
done

mkdir -vp $TMPDIR
#mkdir -vp $TFTPROOT
#mkdir -vp $TFTPROOT/boot-screens
#mkdir -vp $TFTPROOT/pxelinux.cfg
#
##############################################
#cp $SKELETON/pxelinux.0 $TFTPROOT/
cp -ra $SKELETON $TFTPROOT
#On redémarre de tftp
/etc/init.d/tftpd-hpa restart
##############################################


cat > $TFTPROOT/pxelinux.cfg/default << EOF
include /boot-screens/menu.cfg
default /boot-screens/vesamenu.c32
prompt 0
timeout 0
EOF

cat > $TFTPROOT/boot-screens/menu.cfg << EOF
menu hshift 20
menu width 49

menu title Install-Party Cr@ns
menu background boot-screens/splash.png
menu vshift 18
menu rows 5
menu tabmsgrow 16
menu timeoutrow 17
menu tabmsg Press ENTER to boot or TAB to edit a menu entry
menu autoboot Starting Local System in # seconds

prompt 0


label bootlocal
    menu label ^Boot from local disk
        menu default
        localboot 0
        timeout 200           #timeout which is displayed, Wait 10 seconds unless the user types somethin
        totaltimeout 1200     #timeout which executes the default definitely, always boot after 2 minutes

EOF

###########################
#      sysrescuecd        #
###########################
for arch in $SYSRCCD_ARCHS; do
    mkdir -p $TFTPROOT/sysrescuecd/$arch/
    wget $WGETOPT -c $SYSRCCD_FTP/image/isolinux/initram.igz -O $TFTPROOT/sysrescuecd/$arch/initram.igz
    wget $WGETOPT -c $SYSRCCD_FTP/image/isolinux/rescue`echo $arch | sed -n 's/amd64/64/p'` -O $TFTPROOT/sysrescuecd/$arch/rescue
done

if [[ $SYSRCCD_ARCHS != "" ]]; then
cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
menu begin sysrescuecd
    menu title Sysrescue Cd
    label mainmenu
        menu label ^Back..
        menu exit
EOF

for arch in $SYSRCCD_ARCHS; do
cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
        label Sysrescuecd $arch
         kernel sysrescuecd/$arch/rescue
         append initrd=sysrescuecd/$arch/initram.igz dodhcp setkmap=fr boothttp=$SYSRCCD_HTTP/image/sysrcd.dat --
EOF
done

cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
menu end
EOF
fi
###########################
#     Fin sysrescuecd     #
###########################


###########################
#         DEBIAN          #
###########################

for dist in $DEBIAN_DISTS; do
    for arch in $DEBIAN_ARCHS; do
        wget $WGETOPT -c $DEBIAN_FTP/$dist/main/installer-$arch/current/images/netboot/netboot.tar.gz -O $TMPDIR/netboot-debian-$dist-$arch.tar.gz
        mkdir -p $TMPDIR/netboot-debian-$dist-$arch/
        tar zxf $TMPDIR/netboot-debian-$dist-$arch.tar.gz -C $TMPDIR/netboot-debian-$dist-$arch/
        mkdir -p $TFTPROOT/debian-$dist/$arch
        cp $TMPDIR/netboot-debian-$dist-$arch/debian-installer/$arch/initrd.gz $TFTPROOT/debian-$dist/$arch
        cp $TMPDIR/netboot-debian-$dist-$arch/debian-installer/$arch/linux $TFTPROOT/debian-$dist/$arch
        #~ wget $WGETOPT -c $DEBIAN_FTP/$dist/main/installer-$arch/current/images/netboot/gtk/netboot.tar.gz -O $TMPDIR/netboot-debian-gtk-$dist-$arch.tar.gz
        #~ mkdir -p $TMPDIR/netboot-debian-gtk-$dist-$arch/
        #~ tar zxf $TMPDIR/netboot-debian-gtk-$dist-$arch.tar.gz -C $TMPDIR/netboot-debian-gtk-$dist-$arch/
        #~ mkdir -p $TFTPROOT/debian-gtk-$dist/$arch
        #~ cp $TMPDIR/netboot-debian-gtk-$dist-$arch/debian-installer/$arch/initrd.gz $TFTPROOT/debian-gtk-$dist/$arch
        #~ cp $TMPDIR/netboot-debian-gtk-$dist-$arch/debian-installer/$arch/linux $TFTPROOT/debian-gtk-$dist/$arch
        wget $WGETOPT -c $DEBIAN_FTP/$dist/main/installer-kfreebsd-$arch/current/images/netboot/netboot.tar.gz -O $TMPDIR/netboot-debian-kfreebsd-$dist-$arch.tar.gz
        mkdir -p $TMPDIR/netboot-debian-$dist-kfreebsd-$arch/
        tar zxf $TMPDIR/netboot-debian-kfreebsd-$dist-$arch.tar.gz -C $TMPDIR/netboot-debian-$dist-kfreebsd-$arch/
        mkdir -p $TFTPROOT/debian-$dist/kfreebsd-$arch/
                cp -r $TMPDIR/netboot-debian-$dist-kfreebsd-$arch/* $TFTPROOT/debian-$dist/kfreebsd-$arch/
    done
done

cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
menu begin debian
      menu title Debian
      label mainmenu
              menu label ^Back..
              menu exit
EOF

for dist in $DEBIAN_DISTS; do
cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
      menu begin debian-$dist
              menu title Debian $dist
              label mainmenu
                      menu label ^Back..
                      menu exit
EOF
for arch in $DEBIAN_ARCHS; do
cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
              menu begin debian-$dist-$arch
                      menu title Debian $dist $arch
                      label mainmenu
                              menu label ^Back..
                              menu exit
                      DEFAULT install
                      LABEL install
                              kernel debian-$dist/$arch/linux
                              append vga=normal initrd=debian-$dist/$arch/initrd.gz --
                      LABEL expert
                              kernel debian-$dist/$arch/linux
                              append priority=low vga=normal initrd=debian-$dist/$arch/initrd.gz --
                      LABEL rescue
                              kernel debian-$dist/$arch/linux
                              append vga=normal initrd=debian-$dist/$arch/initrd.gz rescue/enable=true --
                      LABEL auto
                              kernel debian-$dist/$arch/linux
                              append auto=true priority=critical vga=normal initrd=debian-$dist/$arch/initrd.gz --
              menu end
EOF
done
for arch in $DEBIAN_ARCHS; do
cat >> $TFTPROOT/boot-screens/menu.cfg <<EOF
              LABEL Debian $dist kfreebsd-$arch
                    kernel debian-$dist/kfreebsd-$arch/grub2pxe
EOF
done
cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
      menu end

EOF
#~ cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
      #~ menu begin debian-gtk-$dist
              #~ menu title Debian GTK $dist
              #~ label mainmenu
                      #~ menu label ^Back..
                      #~ menu exit
#~ EOF
#~ for arch in $DEBIAN_ARCHS; do
#~ cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
              #~ menu begin debian-gtk-$dist-$arch
                      #~ menu title Debian GTK $dist $arch
                      #~ label mainmenu
                              #~ menu label ^Back..
                              #~ menu exit
                      #~ DEFAULT install
                      #~ LABEL install
                              #~ kernel debian-gtk-$dist/$arch/linux
                              #~ append vga=normal initrd=debian-gtk-$dist/$arch/initrd.gz --
                      #~ LABEL expert
                              #~ kernel debian-gtk-$dist/$arch/linux
                              #~ append priority=low vga=normal initrd=debian-gtk-$dist/$arch/initrd.gz --
                      #~ LABEL rescue
                              #~ kernel debian-gtk-$dist/$arch/linux
                              #~ append vga=normal initrd=debian-gtk-$dist/$arch/initrd.gz rescue/enable=true --
                      #~ LABEL auto
                              #~ kernel debian-gtk-$dist/$arch/linux
                              #~ append auto=true priority=critical vga=normal initrd=debian-gtk-$dist/$arch/initrd.gz --
              #~ menu end
#~ EOF
#~ done
#~ cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
      #~ menu end

#~ EOF
done

cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
menu end
EOF
###########################
#       fin DEBIAN        #
###########################


###########################
#     DEBIAN BACKPORT     #
###########################

#rm -r $TMPDIR/netboot-debian-backport-* || true;
for dist in $DEBIAN_BACKPORT_DISTS; do
    for arch in $DEBIAN_BACKPORT_ARCHS; do
	url=`wget $WGETOPT $DEBIAN_BACKPORT_FTP/$dist/ -O- | grep netboot | grep $arch | sort | tail -n 1 | sed 's/">/ /g;s/href="//;s@</a>@@' | awk '{print $6}'`
        wget $WGETOPT -c $url -O $TMPDIR/netboot-debian-backport-$dist-$arch.tar.gz
        mkdir -p $TMPDIR/netboot-debian-backport-$dist-$arch/
        tar zxf $TMPDIR/netboot-debian-backport-$dist-$arch.tar.gz -C $TMPDIR/netboot-debian-backport-$dist-$arch/
        mkdir -p $TFTPROOT/debian-backport-$dist/$arch
        cp $TMPDIR/netboot-debian-backport-$dist-$arch/debian-installer/$arch/initrd.gz $TFTPROOT/debian-backport-$dist/$arch
        cp $TMPDIR/netboot-debian-backport-$dist-$arch/debian-installer/$arch/linux $TFTPROOT/debian-backport-$dist/$arch
    done
done

if [[ $DEBIAN_BACKPORT_DISTS != "" ]]; then
cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
menu begin debian-backport
      menu title Debian Backport
      label mainmenu
              menu label ^Back..
              menu exit
EOF

for dist in $DEBIAN_BACKPORT_DISTS; do
cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
      menu begin debian-backport-$dist
              menu title Debian Backport $dist
              label mainmenu
                      menu label ^Back..
                      menu exit
EOF
for arch in $DEBIAN_BACKPORT_ARCHS; do
cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
              menu begin debian-backport-$dist-$arch
                      menu $arch
                      label mainmenu
                              menu label ^Back..
                              menu exit
                      DEFAULT install
                      LABEL install
                              kernel debian-backport-$dist/$arch/linux
                              append vga=normal initrd=debian-backport-$dist/$arch/initrd.gz --
                      LABEL expert
                              kernel debian-backport-$dist/$arch/linux
                              append priority=low vga=normal initrd=debian-backport-$dist/$arch/initrd.gz --
                      LABEL rescue
                              kernel debian-backport-$dist/$arch/linux
                              append vga=normal initrd=debian-backport-$dist/$arch/initrd.gz rescue/enable=true --
                      LABEL auto
                              kernel debian-backport-$dist/$arch/linux
                              append auto=true priority=critical vga=normal initrd=debian-backport-$dist/$arch/initrd.gz --
              menu end
EOF
done
cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
      menu end

EOF

done

cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
menu end
EOF
fi;
###########################
#   fin DEBIAN BACKPORT   #
###########################

###########################
#         UBUNTU          #
###########################

for dist in $UBUNTU_DISTS; do
    for arch in $UBUNTU_ARCHS; do
        wget $WGETOPT -c $UBUNTU_FTP/$dist/main/installer-$arch/current/images/netboot/netboot.tar.gz -O $TMPDIR/netboot-ubuntu-$dist-$arch.tar.gz
        mkdir -p $TMPDIR/netboot-ubuntu-$dist-$arch/
        tar zxf $TMPDIR/netboot-ubuntu-$dist-$arch.tar.gz -C $TMPDIR/netboot-ubuntu-$dist-$arch/
        mkdir -p $TFTPROOT/ubuntu-$dist/$arch
        cp $TMPDIR/netboot-ubuntu-$dist-$arch/ubuntu-installer/$arch/initrd.gz $TFTPROOT/ubuntu-$dist/$arch
        cp $TMPDIR/netboot-ubuntu-$dist-$arch/ubuntu-installer/$arch/linux $TFTPROOT/ubuntu-$dist/$arch
    done
done

for type in $UBUNTU_LIVE_TYPE; do
    for dist in $UBUNTU_LIVE; do
        for arch in $UBUNTU_LIVE_ARCHS; do
            if test -e $ISODIR/ubuntu/$type-$dist-desktop-$arch.iso; then
    	        mkdir -p $TFTPROOT/livecd/ubuntu/$type-$dist-$arch
    	        mount -o loop $ISODIR/ubuntu/$type-$dist-desktop-$arch.iso $TFTPROOT/livecd/ubuntu/$type-$dist-$arch
            fi
    	done
    done
done
/etc/init.d/nfs-kernel-server start

cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
menu begin ubuntu-livecd
      menu title Ubuntu LiveCd
      label mainmenu
              menu label ^Back..
              menu exit
EOF
for type in $UBUNTU_LIVE_TYPE; do
cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
       menu begin ubuntu-livecd-$type
             menu title $type
             label mainmenu
                   menu label ^Back..
                   menu exit
EOF
for dist in $UBUNTU_LIVE; do
for arch in $UBUNTU_LIVE_ARCHS; do
if test -e $ISODIR/ubuntu/$type-$dist-desktop-$arch.iso; then
if [ -f $TFTPROOT/livecd/ubuntu/$type-$dist-$arch/casper/vmlinuz ]; then
cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
                      LABEL $type Livecd $dist $arch
                      KERNEL /livecd/ubuntu/$type-$dist-$arch/casper/vmlinuz
                      APPEND root=/dev/nfs boot=casper netboot=nfs nfsroot=$OWN_IP:$TFTPROOT/livecd/ubuntu/$type-$dist-$arch initrd=livecd/ubuntu/$type-$dist-$arch/casper/initrd.lz --

EOF
else
cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
                      LABEL $type Livecd $dist $arch
                      KERNEL /livecd/ubuntu/$type-$dist-$arch/casper/vmlinuz.efi
                      APPEND root=/dev/nfs boot=casper netboot=nfs nfsroot=$OWN_IP:$TFTPROOT/livecd/ubuntu/$type-$dist-$arch initrd=livecd/ubuntu/$type-$dist-$arch/casper/initrd.lz --

EOF
fi
fi
done
done
cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
menu end
EOF
done

cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
menu end
EOF
















cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
menu begin ubuntu
      menu title Ubuntu
      label mainmenu
              menu label ^Back..
              menu exit
EOF

for dist in $UBUNTU_DISTS; do
cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
      menu begin ubuntu-$dist
              menu title Ubuntu $dist
              label mainmenu
                      menu label ^Back..
                      menu exit
EOF
for arch in $UBUNTU_ARCHS; do
cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
              menu begin ubuntu-$dist-$arch
                      menu title Ubuntu $dist $arch
                      label mainmenu
                              menu label ^Back..
                              menu exit
                      DEFAULT install
                      LABEL install
                              kernel ubuntu-$dist/$arch/linux
                              append vga=normal initrd=ubuntu-$dist/$arch/initrd.gz --
                      LABEL expert
                              kernel ubuntu-$dist/$arch/linux
                              append priority=low vga=normal initrd=ubuntu-$dist/$arch/initrd.gz --
                      LABEL rescue
                              kernel ubuntu-$dist/$arch/linux
                              append vga=normal initrd=ubuntu-$dist/$arch/initrd.gz rescue/enable=true --
                      LABEL auto
                              kernel ubuntu-$dist/$arch/linux
                              append auto=true priority=critical vga=normal initrd=ubuntu-$dist/$arch/initrd.gz --
              menu end
EOF
done
cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
      menu end

EOF
done

cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
menu end
EOF
###########################
#       fin UBUNTU        #
###########################

###########################
#        Mandriva         #
###########################

if [[ $MANDRIVA_DISTS != "" ]]; then
for dist in $MANDRIVA_DISTS; do
    for arch in $MANDRIVA_ARCHS; do
        mkdir -p $TFTPROOT/mandriva-$dist/$arch/
        wget $WGETOPT -c $MANDRIVA_FTP/$dist/$arch/isolinux/alt0/all.rdz -O $TFTPROOT/mandriva-$dist/$arch/initrd.img
        wget $WGETOPT -c $MANDRIVA_FTP/$dist/$arch/isolinux/alt0/vmlinuz -O $TFTPROOT/mandriva-$dist/$arch/vmlinuz
    done
done

cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
menu begin mandriva
      menu title Mandriva
      label mainmenu
              menu label ^Back..
              menu exit
EOF

for dist in $MANDRIVA_DISTS; do
    cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
    menu begin mandriva-$dist
               menu title Mandriva $dist
               label mainmenu
                       menu label ^Back..
                       menu exit

EOF

    for arch in $MANDRIVA_ARCHS; do
        cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
        menu begin mandriva-$dist-$arch
                       menu title Mandriva $dist $arch
                       label mainmenu
                               menu label ^Back..
                               menu exit
                       label install
                               menu label ^Install
                               kernel mandriva-$dist/$arch/vmlinuz
                               append initrd=mandriva-$dist/$arch/initrd.img ks=http://$OWN_IP/pxe/ks.mandriva-$dist-$arch.cfg ramdisk_size=8192
        menu end
EOF
        cat > $KSROOT/ks.mandriva-$dist-$arch.cfg << EOF
install
url --url $MANDRIVA_FTP/$dist/$arch/
keyboard fr-latin1
EOF
    done

    cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
    menu end
EOF
done

cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
menu end
EOF

fi
###########################
#      fin Mandriva       #
###########################

###########################
#          CentOS         #
###########################

for dist in $CENTOS_DISTS; do
    for arch in $CENTOS_ARCHS; do
        mkdir -p $TMPDIR/centos-$dist/$arch/
        wget $WGETOPT -c $CENTOS_FTP/$dist/os/$arch/images/pxeboot/initrd.img -O $TMPDIR/centos-$dist/$arch/initrd.img
        wget $WGETOPT -c $CENTOS_FTP/$dist/os/$arch/images/pxeboot/vmlinuz -O $TMPDIR/centos-$dist/$arch/vmlinuz
    done
done
cp -r  $TMPDIR/centos-* $TFTPROOT/

cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
menu begin centos
      menu title CentOS
      label mainmenu
              menu label ^Back..
              menu exit
EOF

for dist in $CENTOS_DISTS; do
    cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
    menu begin centos-$dist
               menu title CentOS $dist
               label mainmenu
                       menu label ^Back..
                       menu exit

EOF

    for arch in $CENTOS_ARCHS; do
        cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
        menu begin centos-$dist-$arch
                       menu title CentOS $dist $arch
                       label mainmenu
                               menu label ^Back..
                               menu exit
                       label install
                               menu label ^Install
                               kernel centos-$dist/$arch/vmlinuz
                               append ksdevice=eth0 console=tty0 initrd=centos-$dist/$arch/initrd.img ks=http://$OWN_IP/pxe/ks.centos-$dist-$arch.cfg ramdisk_size=8192
        menu end
EOF
        cat > $KSROOT/ks.centos-$dist-$arch.cfg << EOF
install
url --url $CENTOS_FTP/$dist/os/$arch/
lang en_US.UTF-8
langsupport --default en_US.UTF-8 en_US.UTF-8 fr_FR.UTF-8
keyboard fr-latin1
EOF
    done

    cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
    menu end
EOF
done

cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
menu end
EOF
###########################
#       fin CentOS        #
###########################

###########################
#         Fedora          #
###########################

for dist in $FEDORA_DISTS; do
    for arch in $FEDORA_ARCHS; do
        mkdir -p $TMPDIR/fedora-$dist/$arch/
                wget $WGETOPT -c $FEDORA_FTP/releases/$dist/Fedora/$arch/os/images/pxeboot/initrd.img -O $TMPDIR/fedora-$dist/$arch/initrd.img ||\
        wget $WGETOPT -c $FEDORA_FTP/development/$dist/$arch/os/images/pxeboot/initrd.img -O $TMPDIR/fedora-$dist/$arch/initrd.img
                wget $WGETOPT -c $FEDORA_FTP/releases/$dist/Fedora/$arch/os/images/pxeboot/vmlinuz -O $TMPDIR/fedora-$dist/$arch/vmlinuz ||\
        wget $WGETOPT -c $FEDORA_FTP/development/$dist/$arch/os/images/pxeboot/vmlinuz -O $TMPDIR/fedora-$dist/$arch/vmlinuz
    done
done
cp -r $TMPDIR/fedora-* $TFTPROOT/

cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
menu begin fedora
      menu title Fedora
      label mainmenu
              menu label ^Back..
              menu exit
EOF

for dist in $FEDORA_DISTS; do
    cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
    menu begin fedora-$dist
               menu title Fedora $dist
               label mainmenu
                       menu label ^Back..
                       menu exit

EOF

    for arch in $FEDORA_ARCHS; do
        cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
        menu begin fedora-$dist-$arch
                       menu title Fedora $dist $arch
                       label mainmenu
                               menu label ^Back..
                               menu exit
                       label install
                               menu label ^Install
                               kernel fedora-$dist/$arch/vmlinuz
                               append initrd=fedora-$dist/$arch/initrd.img repo=$FEDORA_FTP/releases/$dist/Fedora/$arch/os/
        menu end
EOF
        #~ cat > $KSROOT/ks.fedora-$dist-$arch.cfg << EOF
#~ install
#~ url --url $FEDORA_FTP/releases/$dist/Fedora/$arch/os/
#~ EOF
    done

    cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
    menu end
EOF
done

cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
menu end
EOF
###########################
#        Fin Fedora       #
###########################

###########################
#        openSUSE         #
###########################
for dist in $OPENSUSE_DISTS; do
    mkdir -p $TMPDIR/opensuse-$dist/$arch/
    wget $WGETOPT $OPENSUSE_FTP/$dist/repo/oss/boot/$arch/loader/linux -O $TMPDIR/opensuse-$dist/$arch/linux
    wget $WGETOPT $OPENSUSE_FTP/$dist/repo/oss/boot/$arch/loader/initrd -O $TMPDIR/opensuse-$dist/$arch/initrd
done
cp -r $TMPDIR/opensuse-* $TFTPROOT/

cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
menu begin opensuse
      menu title Opensuse
      label mainmenu
              menu label ^Back..
              menu exit
EOF

for dist in $OPENSUSE_DISTS; do
cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
    menu begin opensuse-$dist
               menu title openSUSE $dist
               label mainmenu
                       menu label ^Back..
                       menu exit
EOF
    for arch in $OPENSUSE_ARCHS; do
cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
        menu begin opensuse-$dist-$arch
                       menu title Opensuse $dist $arch
                       label mainmenu
                               menu label ^Back..
                               menu exit
                       label install
                               menu label ^Install
                               kernel opensuse-$dist/$arch/linux
                               append initrd=opensuse-$dist/$arch/initrd splash=silent vga=0x314 showopts install=http://download.opensuse.org/factory/repo/oss/ --
      menu end
EOF
    done

cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
    menu end
EOF
done

cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
menu end

EOF

###########################
#        Fin openSUSE     #
###########################

###########################
#        FreeBSD          #
###########################
if [[ $FREEBSD_DISTS != "" ]]; then
for dist in $FREEBSD_DISTS; do
    for arch in $FREEBSD_ARCHS; do
        mkdir -p $TMPDIR/freebsd-$dist/$arch/
                wget $WGETOPT -c $FREEBSD_FTP/releases/$arch/ISO-IMAGES/$dist/FreeBSD-$dist-RELEASE-$arch-bootonly.iso -O $TMPDIR/freebsd-$dist/$arch/bootonly.iso
    done
done
cp -r $TMPDIR/freebsd-* $TFTPROOT/

cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
menu begin freebsd
      menu title FreeBSD
      label mainmenu
              menu label ^Back..
              menu exit
EOF

for dist in $FREEBSD_DISTS; do
    cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
    menu begin freebsd-$dist
               menu title FreeBSD $dist
               label mainmenu
                       menu label ^Back..
                       menu exit

EOF

    for arch in $FREEBSD_ARCHS; do
        cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
        menu begin freebsd-$dist-$arch
                       menu title FreeBSD $dist $arch
                       label mainmenu
                               menu label ^Back..
                               menu exit
                       label install
                               menu label ^Install
                               linux memdisk
                               initrd freebsd-$dist/$arch/bootonly.iso
                               append iso
        menu end
EOF
    done

    cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
    menu end
EOF
done

cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
menu end
EOF
fi
###########################
#      Fin FreeBSD        #
###########################

###########################
#        NetBSD           #
###########################
# Le fichier de boot est le même pour les deux architectures
# On prend toujours le plus récent
#~ mkdir -p $TFTPROOT/netbsd/
#~ dist=4.0.1
#~ wget $WGETOPT -c $NETBSD_FTP/NetBSD-$dist/amd64/installation/misc/pxeboot_ia32.bin -O $TFTPROOT/netbsd/pxeboot_ia32.bin

# Hack moche : la version 5.0.1 ne supporte pas le netboot (le kernel freeze)
#for dist in $NETBSD_DISTS; do
    #~ for arch in $NETBSD_ARCHS; do
        #~ wget $WGETOPT -c $NETBSD_FTP/NetBSD-$dist/$arch/binary/kernel/netbsd-INSTALL.gz -O $TFTPROOT/netbsd/netbsd-$dist-$arch.gz
    #~ done
#done

#~ cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
#~ label NetBSD
    #~ kernel netbsd/pxeboot_ia32.bin

#~ EOF
###########################
#        Fin NetBSD       #
###########################

###########################
#        OpenBSD          #
###########################
if [[ $OPENBSD_DIST != "" ]]; then
mkdir -p $TMPDIR/openbsd
for dist in $OPENBSD_DIST; do
    for arch in $OPENBSD_ARCHS; do
        # On ne garde que le dernier fichier de boot
        wget $WGETOPT -c $OPENBSD_FTP/$dist/$arch/pxeboot -O $TMPDIR/openbsd/openbsd.$arch.0
        wget $WGETOPT -c $OPENBSD_FTP/$dist/$arch/bsd.rd -O $TMPDIR/openbsd/bsd.rd-$dist.$arch
    done
done
cp -r $TMPDIR/openbsd/ $TFTPROOT/

cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
menu begin openbsd
    menu title OpenBSD
    label mainmenu
        menu label ^Back..
        menu exit
EOF
for arch in $OPENBSD_ARCHS; do
cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
    label OpenBSD $arch
         kernel openbsd/pxeboot.$arch.0
EOF
cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
menu end
EOF
done
fi
###########################
#        Fin OpenBSD      #
###########################



echo "Fini"
