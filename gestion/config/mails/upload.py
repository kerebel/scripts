#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Templates des mails envoyés en cas d'upload.
TODO: à migrer dans /usr/scripts/gestion/mail/templates
"""

#: Envoyé à la ML disconnect@ en cas de dépassement de la limite soft (désactivé)
message_disconnect_soft = u"""From: %(from)s
To: %(to)s
Subject: %(proprio)s uploade
Content-Type: text/plain; charset="utf-8"

%(proprio)s uploade actuellement %(upload)s Mio.


--\u0020
Message créé par deconnexion.py"""

#: Envoyé à la ML disconnect@ en cas de dépassement de la limite hard plusieurs fois
message_disconnect_multi = u"""From: %(from)s
To: %(to)s
Subject: %(proprio)s a =?utf-8?q?=C3=A9t=C3=A9_brid=C3=A9?= %(nbdeco)d fois pour upload en un mois !
Content-Type: text/plain; charset="utf-8"

L'adhérent %(proprio)s a été bridé %(nbdeco)d fois pour upload en un mois !

Le PS a été généré et se trouve sur zamok :
%(ps)s

--\u0020
Message créé par deconnexion.py"""
