#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Définition de variables de configuration et de comportement des scripts Cr@ns """
import time
import datetime

# Fichier généré à partir de bcfg2
from config_srv import adm_only, role

##### Gestion des câblages
# Selon la date, on met :
# -ann_scol : Année scolaire en cours
# -periode_transitoire : on accepte ceux qui ont payé l'année dernière

# Ne modifier que les dates !
dat = time.localtime()
if dat[1] < 8 or dat[1] == 8 and dat[2] < 16:
    # Si pas encore début août, on est dans l'année précédente
    ann_scol = dat[0]-1
    periode_transitoire = False
    # sinon on change d'année
elif dat[1] < 10:
    # Si pas encore octobre, les gens ayant payé l'année précédente sont
    # acceptés
    ann_scol = dat[0]
    periode_transitoire = True
else:
    # Seulement ceux qui ont payé cette année sont acceptés
    ann_scol = dat[0]
    periode_transitoire = False

debut_periode_transitoire = time.mktime(time.strptime("%s/08/16 00:00:00" % (ann_scol,), "%Y/%m/%d %H:%M:%S"))
fin_periode_transitoire = time.mktime(time.strptime("%s/09/30 23:59:59" % (ann_scol,), "%Y/%m/%d %H:%M:%S"))

## Bloquage si carte d'étudiants manquante pour l'année en cours
# /!\ Par sécurité, ces valeurs sont considérées comme False si
#     periode_transitoire est True
# Soft (au niveau du Squid)
bl_carte_et_actif = True
# Hard (l'adhérent est considéré comme paiement pas ok)
bl_carte_et_definitif = True

#Sursis pour les inscription après le 1/11 pour fournir la carte étudiant
sursis_carte=8*24*3600

# Gel des cableurs pas a jour de cotisation
# Les droits ne sont pas retires mais il n'y a plus de sudo
bl_vieux_cableurs = False

##Création de comptes
# Gid des comptes créés
gid = 100
club_gid = 120
mailgroup = 8
default_rights = 0755
default_mail_rights = 0700
quota_soft = 16000000
quota_hard = 17000000
# Shell
login_shell='/bin/zsh'
club_login_shell='/usr/bin/rssh'
# Longueur maximale d'un login
maxlen_login=25

shells_possibles = [u'/bin/csh',
 u'/bin/sh', # tout caca
 u'/bin/dash', # un bash light
 u'/usr/bin/rc',
 u'/usr/bin/ksh', # symlink vers zsh
 u'/bin/ksh', # symlink vers zsh
 u'/usr/bin/tcsh', # TENEX C Shell (csh++)
 u'/bin/tcsh', # TENEX C Shell (csh++)
 u'/bin/bash', # the Bourne-Again SHell
 u'/bin/zsh', # the Z shell
 u'/usr/bin/zsh', # the Z shell
 u'/usr/bin/screen',
 u'/bin/rbash', # Bash restreint
 u'/usr/bin/rssh', # restricted secure shell allowing only scp and/or sftp
 u'/usr/local/bin/badPassSh', # demande de changer de mot de passe
 u'/usr/bin/passwd', # idem
 u'/usr/local/bin/disconnect_shell', # déconnexion crans
 u'/usr/scripts/surveillance/disconnect_shell', # idem
 u'/usr/sbin/nologin', # This account is currently not available.
 u'/bin/false', # vraiement méchant
 u'/usr/bin/es', # n'exsite plus
 u'/usr/bin/esh', # n'existe plus
 u'', # le shell vide pour pouvoir les punis
]

shells_gest_crans_order = ["zsh", "bash", "tcsh", "screen", "rbash", "rssh",
                           "badPassSh", "disconnect_shell"]
shells_gest_crans = {
    "zsh":   {"path":"/bin/zsh",        "desc":"Le Z SHell, shell par defaut sur zamok"},
    "bash":  {"path":"/bin/bash",       "desc":"Le Boune-Again SHell, shell par defaut de la plupart des linux"},
    "tcsh":  {"path":"/bin/tcsh",       "desc":"C SHell ++"},
    "screen":{"path":'/usr/bin/screen', "desc":"Un gestionnaire de fenêtre dans un terminal"},
    "rbash": {"path":"/bin/rbash",      "desc":"Un bash très restreint, voir man rbash"},
    "rssh":  {"path":"/usr/bin/rssh",   "desc":"Shell ne permetant que les transferts de fichiers via scp ou sftp"},
    "badPassSh":{"path":"/usr/local/bin/badPassSh", "desc":"Demande de changer de mot de passe à la connexion"},
    "disconnect_shell":{"path":"/usr/local/bin/disconnect_shell", "desc":"Shell pour les suspensions de compte avec message explicatif"},
}
# Quels droits donnent l'appartenance à quel groupe Unix ?
droits_groupes = {'adm' : [u'Nounou'],
                  'respbats' : [u'Imprimeur', u'Cableur', u'Nounou'],
                  'moderateurs' : [u'Moderateur'],
                  'disconnect' : [u'Bureau'],
                  'imprimeurs' : [u'Imprimeur', u'Nounou', u'Tresorier'],
                  'bureau' : [u'Bureau'],
                  'webadm' : [u'Webmaster'],
                  'webradio' : [u'Webradio'],
                 }

####### Les modes de paiement accepté par le crans

modePaiement = ['liquide', 'paypal', 'solde', 'cheque', 'carte']

####### Les ML
# Le + devant un nom de ML indique une synchronisation
# ML <-> fonction partielle : il n'y a pas d'effacement automatique
# des abonnés si le droit est retiré
droits_mailing_listes = {'roots' : [ u'Nounou', u'Apprenti'],
                         'mailman' : [ u'Nounou'],
                         '+nounou' : [ u'Nounou', u'Apprenti'],
                         'respbats' : [ u'Cableur', u'Nounou', u'Bureau'],
                         'moderateurs' : [ u'Moderateur', u'Bureau'],
                         'disconnect' : [ u'Nounou', u'Bureau'],
                         'impression' : [ u'Imprimeur'],
                         'bureau' : [u'Bureau'],
                         'tresorier' : [u'Tresorier'],
                         '+ca' : [u'Bureau', u'Apprenti', u'Nounou', u'Cableur'],

                         '+federez' : [u'Bureau', u'Apprenti', u'Nounou'],
                         '+install-party' : [u'Bureau', u'Apprenti', u'Nounou'],

                         # Correspondance partielle nécessaire... Des adresses non-crans sont inscrites à ces ML.
                         '+dsi-crans' : [u'Nounou', u'Bureau'],
                         '+crous-crans' : [u'Nounou', u'Bureau'],

                         '+wrc' : [u'Webradio'],
                         }

#: Répertoire de stockage des objets détruits
cimetiere = '/home/cimetiere'

#: Adresses mac utiles
mac_komaz = 'a0:d3:c1:00:f4:04'
mac_titanic = 'aa:73:65:63:6f:76'

#: Serveur principal de bcfg2
bcfg2_main = "bcfg2.adm.crans.org"

#: Fichier de mapping lun/nom de volume iscsi
ISCSI_MAP_FILE = "/usr/scripts/var/iscsi_names_%s.py"

#: Algorithmes de hashage pour le champ SSHFP
# format : { algorithm : (IANA_id, ssh_algo) }
# où algorithm est tel qu'il apparait dans les fichiers /etc/ssh/ssh_host_%s_key.pub % algorithm
# IANA_id correspond à l'entier attribué par l'IANA pour l'algorithm dans les champs DNS SSHFP
# ssh_algo correspond a la première chaine de caractères donnant le nom de l'algorithme de chiffrement lorsque la clef ssh est dans le format openssh (algo key comment)
sshfp_algo = {
        "rsa" : (1, "ssh-rsa"),
        "dsa" : (2, "ssh-dss"),
        "ecdsa-256" : (3, "ecdsa-sha2-nistp256"),
        "ecdsa-384" : (3, "ecdsa-sha2-nistp384"),
        "ecdsa-521" : (3, "ecdsa-sha2-nistp521"),
        "ecdsa"     : (3, "ecdsa-sha2-nistp521"),
    }

sshfs_ralgo = {}
for key,value in sshfp_algo.items():
    sshfs_ralgo[value[1]] = (value[0], key)

sshfp_hash = {
   "sha1" : 1,
   "sha256" : 2,
}

sshkey_max_age=2*(365.25*24*3600)

sshkey_size = {
    'rsa':4096,
    'dsa':1024,
    'ecdsa':521,
}

#: Nombre de jours après le passage en chambre ???? où on supprime les machines
demenagement_delai = 8



#############################
## Paramètres des machines ##
#############################

## >>>>>>>>>>>>>>> La modification des paramètres suivants doit se
## >> ATTENTION >> faire avec précaution, il faut mettre la base à
## >>>>>>>>>>>>>>> jour en parralèle de ces modifs.

# Sous réseaux alloués à chaque type de machine ou bâtiment
# Pour la zone wifi, il faut penser à modifier le /etc/network/interfaces
# de sable, zamok et komaz pour ajouter les zones en plus (et de
# faire en sorte qu'ils prennent effet immédiatement ; c'est important pour
# komaz car c'est la route par défaut mais aussi pour zamok et sable
# à cause de leur firewall et de leur patte wifi.

plage_ens = '138.231.0.0/16'

# NETs_primaires contient une bijection entre des types de machines
# et les plages d'ip qui vont bien. NETs_secondaires contient des
# clefs qui cassent la bijectivité, mais qui peuvent servir.
# NETs est l'union des deux
NETs_primaires = {
         'serveurs' : ['138.231.136.0/24'],
         'adherents' : ['138.231.137.0/24', '138.231.138.0/23', '138.231.140.0/22'],
         'wifi-adh' : ['138.231.144.0/22', '138.231.148.128/25', '138.231.149.0/24', '138.231.150.0/23'],
         'bornes' : ['138.231.148.0/25'],
         'adm' : ['10.231.136.0/24'],
         'personnel-ens' : ['10.2.9.0/24'],
         'gratuit' : ['10.42.0.0/16'],
         'accueil' : ['10.51.0.0/16'],
         'isolement' : ['10.52.0.0/16'],
         'evenementiel' : ['10.231.137.0/24'],
         'multicast' : ['239.0.0.0/8'],
         'ens' : ['138.231.135.0/24'],
         }

NETs_secondaires = {
         'all' : ['138.231.136.0/21', '138.231.144.0/21'],
         'wifi': ['138.231.144.0/21'],
         'fil' : ['138.231.136.0/21'],
        }

NETs = {}
NETs.update(NETs_primaires)
NETs.update(NETs_secondaires)

NETs_regexp = { 'all' : '^138\.231\.1(3[6789]|4[0123456789]|5[01])\.\d+$' }

# Classes de rid
# Merci d'essayer de les faire correspondre avec les réseaux
# ci-dessus...
# De même que pout NETs, primaires c'est pour la bijectivité, et secondaires
# pour les trucs pratiques
rid_primaires = {
    # Rid pour les serveurs
    'serveurs' : [(0, 255),],
    # Rid pour les machines fixes
    'adherents' : [(256, 2047),],
    # Rid pour les machines wifi
    'wifi-adh' : [(2048, 3071), (3200, 4095),],
    # Rid pour les bornes
    'bornes' : [(3072, 3199),],
    # Rid pour machines spéciales
    'special' : [(4096, 6143),],
    # Rid pour les serveurs v6-only
    'serveurs-v6' : [(8192, 10240),],
    # Rid pour les filaires v6-only
    'adherents-v6' : [(16384, 24575),],
    # Rid pour les wifi v6-only
    'wifi-adh-v6' : [(24576, 32767),],
    # Bornes-v6 ?
    'bornes-v6' : [(32768, 33791),],
    # Rid pour les machines du vlan adm
    'adm-v6' : [(49152, 51199),],
    # Rid pour les machines du vlan adm
    'adm' : [(51200, 53247),],
    # Mid pour les machines du vlan gratuit
    'gratuit' : [(53248, 55295),],
    # Rid pour les machines des personnels ens
    'personnel-ens' : [(55296, 55551),],
    # Un unique rid pour les machines multicast
    'multicast' : [(65535, 65535),],
    }

rid_secondaires = {
    # Rid pour les machines filaire ipv4
    'fil' : [(0, 2047),],
    'wifi' : [(2048, 4095),],
    }

rid = {}
rid.update(rid_primaires)
rid.update(rid_secondaires)

# rid pour les machines spéciales (classe 'special' ci-dessus)
rid_machines_speciales = {
    # freebox.crans.org
    4096: '82.225.39.54',
    # ovh.crans.org
    4097: '91.121.84.138',
    # soyouz.crans.org
    4098: '91.121.179.40',
}

ipv6_machines_speciales = {
    # freebox
    4096: '2a01:e35:2e12:7360:a873:65ff:fe63:6f77',
    # ovh
    4097: '2001:41d0:1:898a::1',
    # soyouz
    4098: '2001:41d0:1:f428::1',
}

# Les préfixes ipv6
prefix = {  'subnet' : [ '2a01:240:fe3d::/48' ],
            'serveurs' : [ '2a01:240:fe3d:4::/64' ],
            'adherents' : [ '2a01:240:fe3d:4::/64' ],
            'fil' : [ '2a01:240:fe3d:4::/64' ],
            'adm' : [ '2a01:240:fe3d:c804::/64' ],
            'adm-v6' : [ '2a01:240:fe3d:c804::/64' ],
            'wifi' : [ '2a01:240:fe3d:c04::/64' ],
            'serveurs-v6' : [ '2a01:240:fe3d:c04::/64' ],
            'adherents-v6' : [ '2a01:240:fe3d:4::/64' ],
            'wifi-adh-v6' : [ '2a01:240:fe3d:c04::/64' ],
            'personnel-ens' : [ '2a01:240:fe3d:4::/64' ],
            'sixxs2' : [ '2a01:240:fe00:68::/64' ],
            'evenementiel' : [ '2a01:240:fe3d:d2::/64' ],
            'bornes' : [ '2a01:240:fe3d:c04::/64' ],
            'wifi-adh' : [ '2a01:240:fe3d:c04::/64' ],
            'v6only' : [ '2001:470:c8b9:a4::/64' ],
            }

# Domaines dans lesquels les machines sont placées suivant leur type
domains = { 'machineFixe': 'crans.org',
            'machineCrans': 'crans.org',
            'machineWifi': 'wifi.crans.org',
            'borneWifi': 'wifi.crans.org' }

# VLans
vlans = {
    # VLan d'administration
    'adm' : 2,
    # VLan pour le wifi
    'wifi' : 3,
    # VLan pour le wifi de l'ens
    'hotspot' : 4,
    # VLan des gens qui paient
    'adherent' : 1,
    # VLan des inconnus
    'accueil' : 7,
    # VLan de la connexion gratuite
    'gratuit' : 6,
    'radin' : 6,
    'v6only': 6,
    # Vlan isolement
    'isolement' : 9,
    # Vlan de tests de chiffrement DSI
    'chiffrement': 11,
    # VLan des appartements de l'ENS
    'appts': 21,
    # Vlan evenementiel (install-party, etc)
    'event': 10,
    # Vlan zone routeur ens (zrt)
    'zrt': 1132,
    # iSCSI (pour exporter la baie de disque sur vo)
    'iscsi': 42,
    # freebox (pour faire descendre la connexion au 0B)
    'freebox': 8,
    }

filter_policy = { 'komaz' : { 'policy_input' : 'ACCEPT',
                              'policy_forward' : 'ACCEPT',
                              'policy_output' : 'ACCEPT'
                            },
                  'zamok' : { 'policy_input' : 'ACCEPT',
                              'policy_forward' : 'DROP',
                              'policy_output' : 'ACCEPT'
                            },
                  'default' : { 'policy_input' : 'ACCEPT',
                              'policy_forward' : 'ACCEPT',
                              'policy_output' : 'ACCEPT'
                            }
                }

# Cf RFC 4890
authorized_icmpv6 = ['echo-request', 'echo-reply', 'destination-unreachable',
        'packet-too-big', 'ttl-zero-during-transit', 'parameter-problem']

output_file = { 4 : '/tmp/ipt_rules',
                6 : '/tmp/ip6t_rules'
              }

file_pickle = { 4 : '/tmp/ipt_pickle',
                6 : '/tmp/ip6t_pickle'
              }

##################################################################################
#: Items de la blackliste
blacklist_items = { u'bloq': u'Blocage total de tous les services',
                    u'carte_etudiant': u'Carte etudiant manquante',
                    u'paiement': u'Paiement manquant cette année',
                    u'virus': u'Passage en VLAN isolement',
                    u'upload': u"Bridage du débit montant vers l'extérieur",
                    u'p2p': u"Blocage total de l'accès à l'extérieur",
                    u'autodisc_virus': u'Autodisconnect pour virus',
                    u'autodisc_upload': u'Autodisconnect pour upload',
                    u'autodisc_p2p': u'Autodisconnect pour P2P',
                    u'ipv6_ra': u'Isolement pour RA',
                    u'mail_invalide': u'Blocage pour mail invalide',
                    u'warez' : u"Présence de contenu violant de droit d'auteur sur zamok",
                  }

#: Blacklistes entrainant une déconnexion complète
blacklist_sanctions = ['warez', 'p2p', 'autodisc_p2p','autodisc_virus','virus', 'bloq', 'paiement']
if bl_carte_et_definitif:
    blacklist_sanctions.append('carte_etudiant')
#: Blacklistes redirigeant le port 80 en http vers le portail captif (avec des explications)
blacklist_sanctions_soft  = ['autodisc_virus','ipv6_ra','mail_invalide','virus',
         'warez', 'p2p', 'autodisc_p2p', 'bloq','carte_etudiant','chambre_invalide', 'paiement']
#: Blacklistes entrainant un bridage de la connexion pour upload
blacklist_bridage_upload = ['autodisc_upload', 'upload']

##################################################################################

adm_users = [ 'root', 'identd', 'daemon', 'postfix', 'freerad', 'amavis',
        'nut', 'respbats', 'list', 'sqlgrey', 'ntpd', 'lp' ]

open_ports = { 'tcp' : '22' }

# Debit max sur le vlan de la connexion gratuite
debit_max_radin = 1000000
debit_max_gratuit = 1000000

###############################
## Vlan accueil et isolement ##
###############################
accueil_route = {
    '138.231.136.1':{'tcp':['80','443', '22'],'hosts':['intranet.crans.org', 'ssh.crans.org', 'zamok.crans.org']},
    '138.231.136.67':{'tcp':['80','443'],'hosts':['www.crans.org', 'wiki.crans.org', 'wifi.crans.org']},
    '138.231.136.98':{'tcp':['20','21','80','111','1024:65535'],'udp':['69','1024:65535'], 'hosts':['ftp.crans.org']},
    '138.231.136.130':{'tcp':['80','443'],'hosts':['intranet2.crans.org']},
    '138.231.136.18':{'tcp':['80','443'],'hosts':['cas.crans.org', 'login.crans.org', 'auth.crans.org']},
    '213.154.225.236':{'tcp':['80','443'], 'hosts':['crl.cacert.org']},
    '213.154.225.237':{'tcp':['80','443'], 'hosts':['ocsp.cacert.org']},
}

dhcp_servers = ['dhcp.adm.crans.org', 'isc.adm.crans.org']
