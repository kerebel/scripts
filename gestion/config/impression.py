# -*- coding: utf-8 -*-

""" Définition des variables d'impression utilisées par les scripts, l'intranet…

Voir aussi: https://wiki.crans.org/ComptesRendusCrans/Imprimante2014?action=recall&rev=9

(imprimante hp, contrat mps)

"""

#: L'imprimante utilisée
imprimante = "hp"


#: Découvert autorisé (en euro)
decouvert = 0.

## Variables de prix (tout est exprimé en centimes)

#: Coût de l'imprimante
#:
#: Donc ammortissement
amm = 2.16

#: Coût d'une feuille A4 : 16.69 euros les 2500 feuilles
c_a4 = 0.668

#: Coût d'une feuille A3 : 53.76 euros les 2500 feuilles
c_a3 = 2.1504

#: Coût d'un transparent : 15.85 euros les 100 transparents
#c_trans = 15.85 + amm

#: Coût d'impression d'une face en couleur
c_face_couleur = 6.84 + amm
#: Coût d'impression d'une face en noir & blanc
c_face_nb = 0.65 + amm

#: Prix d'une agrafe : 200 euros les 15 000
c_agrafe = 1.3333

### Fin des variables de prix

#: L'adresse mail de l'imprimante
From_imprimante = 'impression@crans.org'

#: Informations supplémentaires sur l'état de l'imprimante, affichée sur l'intranet
state_msg = []

