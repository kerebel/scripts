#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Les clef sont un code article
items = {
        'CABLE' : {'designation': u'Cable Ethernet 5m', 'pu': 3, 'imprimeur': False},
        'ADAPTATEUR' : {'designation': u'Adaptateur Ethernet/USB', 'pu': 17, 'imprimeur': False},
        'RELIURE': {'designation': u'Reliure plastique', 'pu': 0.12, 'imprimeur': False},
        'SOLDE':{'designation': u'Rechargement du solde', 'pu':'*', 'imprimeur': False},
}
