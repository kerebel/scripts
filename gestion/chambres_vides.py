#!/bin/bash /usr/scripts/python.sh
# -*- encoding: utf-8 -*-

""" Pour détecter les gens en chambre invalide, les prévenir, et supprimer leurs machines
    en l'absence de réponse. Récupérer des IPs, c'est cool."""

# Codé par b2moo, commenté par 20-100, cr{itiqu|on}é par Nit
# <daniel.stan@crans.org>
# <legallic@crans.org>
# <samir@crans.org>

import datetime
import time
import re
import lc_ldap.shortcuts
from lc_ldap.crans_utils import fromGeneralizedTimeFormat, toGeneralizedTimeFormat
conn = lc_ldap.shortcuts.lc_ldap_admin()

import mail as mail_module
import sys
#: envoyer un mail à chaque adhérent concerné
sendmails = False
if "--mail-all" in sys.argv:
    sendmails = True
from email.header import Header

#: Envoyer un mail à respbats
sendmail_respbats = True
if "--no-mail" in sys.argv:
    sendmail_respbats = False

DEBUG = False
if "--debug" in sys.argv:
    DEBUG = True

import os
import config
year = config.ann_scol
delai = config.demenagement_delai

# On récupère ceux qui n'ont pas payé cette année
now = time.time()
if config.periode_transitoire:
    bad_boys_e_s = conn.search(u'(&(aid=*)(chbre=????)(|(&(paiement=%d)(!(paiement=%d)))(finAdhesion<=%s)(finConnexion<=%s)))' % (year-1, year, toGeneralizedTimeFormat(now), toGeneralizedTimeFormat(now)))
else:
    bad_boys_e_s = conn.search(u'(&(aid=*)(chbre=????)(|(paiement=%d)(finAdhesion>=%s)(finConnexion>=%s)))' % (year, toGeneralizedTimeFormat(now), toGeneralizedTimeFormat(now)))

to_print = []
to_error = []
for clandestin in bad_boys_e_s:
    # On cherche la dernière fois qu'il s'est retrouvé en chambre ????
    for l in clandestin['historique'][::-1]:
        # On récupère la date du dernier changement de chambre
        #  (l'historique est enregistré par ordre chronologique)
        x = re.match("(.*),.* : chbre \((.*) -> \?\?\?\?\)", str(l))
        if x <> None:
            kickout_date = x.group(1)
            exchambre = x.group(2)

    machine_liste = clandestin.machines()
    # On lui accorde un délai
    kickout_date = time.mktime(time.strptime(kickout_date, "%d/%m/%Y %H:%M"))
    delta = now - kickout_date
    ttl = delai*86400 - delta
    if ttl > 0:
        if (sendmails and machine_liste != [] or DEBUG) and (ttl >= (delai - 1)*86400 or 0 < ttl <= 86400):
            # On lui envoie un mail pour le prévenir
            to = clandestin['mail'][0]
            mail = mail_module.generate('demenagement', {"from" : "respbats@crans.org",
                                                         "chambre" : exchambre,
                                                         "jours" : int(ttl/86400) + 1,
                                                         "to" : to,
                                                         "prenom" : clandestin["prenom"][0],
                                                         "nom" : clandestin["nom"][0],
                                                         "lang_info":"English version below"}
                                       ).as_string()
            if DEBUG:
                print mail
            mailer = os.popen("/usr/sbin/sendmail -t", "w")
            mailer.write(mail + "\n.")
            mailer.close()

    else:
        for m in machine_liste:
            try:
                reason = u'Adhérent sans chambre valide depuis %d jours' % delai
                with conn.search(u'mid=%s' % m['mid'][0], mode='w')[0] as m2:
                    m2.delete(reason)
                to_print.append( (clandestin['aid'][0], m['ipHostNumber'][0], m['mid'][0], m['host'][0]) )
            except Exception as e:
                to_error.append((clandestin['aid'][0], m['ipHostNumber'][0], m['mid'][0], m['host'][0], e))

message = u""
if to_print != []:
    # Il s'est passé quelque chose, donc on envoie un mail
    # On regarde le plus grand hostname
    hostnamemaxsize = max([len(str(i[3])) for i in to_print])
    template = u"| %%4s | %%-15s | %%4s | %%-%ss |\n" % (hostnamemaxsize)
    message += u"\nListe des machines supprimées pour chambre invalide depuis plus de %s jours :\n" % delai
    tiret_line = u"+------+-----------------+------+-%s-+\n" % ("-" * hostnamemaxsize)
    message += tiret_line
    message += template % ("aid", "       ip", "mid", (" " * (max((hostnamemaxsize-8)/2,0)) + "hostname"))
    message += tiret_line
    for aid, ip, mid, hostname in to_print:
        message += template % (aid, ip, mid, hostname)

    message += tiret_line
    message += u"\nScore de cette nuit : %s" % (len(to_print))

if to_error != []:
    hostnamemaxsize = max([len(str(i[3])) for i in to_error])
    errormaxsize = max([len(str(i[4])) for i in to_error])
    template = u"| %%4s | %%-15s | %%4s | %%-%ss | %%-%ss |\n" % (hostnamemaxsize, errormaxsize)
    message += u"\n"
    tiret_line = u"+------+-----------------+------+-%s-+-%s-+\n" % ("-" * hostnamemaxsize, "-" * errormaxsize)
    message += u"\nListe des machines dont la supression à échoué :\n"
    message += tiret_line
    message += template % ("aid", "       ip", "mid", (" " * (max((hostnamemaxsize-8)/2,0)) + "hostname"), (" " * (max((errormaxsize-6)/2,0)) + "erreur"))
    for aid, ip, mid, hostname, error in to_error:
        message += template % (aid, ip, mid, hostname, error)
    message += tiret_line

if to_print != [] or to_error != []:
    headers = u"From: respbats@crans.org\nSubject: %s\n" % Header("Machines supprimées pour chambre invalide", "utf8").encode()
    headers += u"Content-Type: text/plain; charset=UTF-8\n"
    headers += u"X-Mailer: /usr/scripts/gestion/chambres_vides.py\n"
    headers += u"To: respbats@crans.org\n"
    mail = headers + "\n" + message
    if sendmails:
        mailer = os.popen("/usr/sbin/sendmail -t", "w")
        mailer.write(mail.encode("utf-8") + "\n.")
        mailer.close()
    else:
        print mail
